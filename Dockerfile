FROM python:latest
COPY . .
RUN pip install -r requirements.txt
ENTRYPOINT ["uvicorn", "main:app", "--reload"]