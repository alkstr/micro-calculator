import functools
from typing import List

from fastapi import FastAPI

app = FastAPI()


class Calculator:
    @staticmethod
    def calculate(operation: str, numbers: List[int]) -> int:
        return Calculator._operations[operation](numbers)

    _operations = {
        '-': lambda nums: functools.reduce(lambda x, y: x - y, nums),
        '+': lambda nums: functools.reduce(lambda x, y: x + y, nums),
        '/': lambda nums: functools.reduce(lambda x, y: x / y, nums),
        '*': lambda nums: functools.reduce(lambda x, y: x * y, nums)
    }


@app.post("/")
async def root(operation: str, numbers: List[int]):
    if len(numbers) < 2:
        return {"error": "Array must contain at least two integer arguments"}
    if operation not in {'-', '+', '/', '*'}:
        return {"error": "Unknown operation"}
    if operation == '/' and 0 in numbers[1:]:
        return {"error": "Division by zero"}

    return {"result": Calculator.calculate(operation, numbers)}


@app.get("/")
async def hello():
    return "Hello, world!"
